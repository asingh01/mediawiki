FROM centos:7

# Installing packages
RUN yum -y install centos-release-scl \
    && yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
    && yum -y install https://rpms.remirepo.net/enterprise/remi-release-7.rpm \
    && yum -y install yum-utils \
    && yum-config-manager --enable remi-php74 \
    && yum -y update \
    && yum -y install php php-cli php-mbstring php-xml php-mysql httpd wget mariadb-server mariadb \
    && systemctl enable httpd

# Installing Application
RUN mkdir /home/mediawiki \
    && cd /home/mediawiki \
    && wget https://releases.wikimedia.org/mediawiki/1.35/mediawiki-1.35.1.tar.gz \
    && wget https://releases.wikimedia.org/mediawiki/1.35/mediawiki-1.35.1.tar.gz.sig

RUN cd /var/www \
    && tar -zxf /home/mediawiki/mediawiki-1.35.1.tar.gz

RUN ln -s /var/www/mediawiki-1.35.1/ /var/www/mediawiki

RUN sed -i -e 's%"/var/www/html"%"/var/www/mediawiki"%g' /etc/httpd/conf/httpd.conf \
    && chown -R apache:apache /var/www/mediawiki-1.35.1 \
    && chown -R apache:apache /var/www/mediawiki

ADD run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh

EXPOSE 80