# Media Wiki Deployment *Terraform/GKE/HELM*

Automated Deployment for Media Wiki

This document explains the deployment of Media Wiki application that have different components to be deployed and the approach for the deployment. The deployment for the Server and the mysql will be performed in a distributed manner to avoid the scenario for a single point of failure in case the any pods fails.

**Dockerfile:** There is a single Dockerfile contained in this repository containing the binary to be executed for the server. It installs the relevant packages and configure the application

**Helm:** The helm chart to deploy the application into kubernetes is contained inside the folder "mediawiki". The template folder consists the templates for kubernetes resources to be deployed.
  Note: The 3 components (server, worker and redis) are been deployed as different resources to achieve a distributed system.
  1) deployment.yaml - It will deploy the server component of the application as a deployment.
  2) service.yaml - The service will expose the pods inside the deployment.
  4) ingress.yaml - It will create an ingress(Load Balancer) that will expose the endpoint to outer world.

 ** MySQL:** The installation of mysql is performed using a public helm chart offered by Helm which is a stable chart and created a mysql Deployment that hosts mysql database.
  (Note: To achieve this I have to add a dependency in Chart.yaml and run "helm dep update" and helm automatically downloads the chart and puts it under charts folder.)
  The values for the server and worker components and the env variables are defined inside values.yaml file.


**Helmfile:** I am using Helmfile for deploying helm as it offers extra features from helm that are very much useful.
Please refer https://github.com/roboll/helmfile for more info.

**Terraform:** The terraform files to create a gke cluster are stored inside the terraform folder.
  To deploy a GKE cluster, The public module for GKE that is offered by Terraform and managed by Google is used. Using this module as it is reliable and saves time as it can be reused and takes care of all the dependent resources that are need to be created(Eg: node pools etc. needed in our case.
  The backend.tf file contains details of the bucket which will store the state file remotely and will also implement state locking so the statefile should not be overridden mistakenly. 
  (Note: Some values are directly been enetered inside gke.tf (due to the time constraint) but can be further variablised to make it more extensible)

**.gitlab-ci.yml** (Triggering the Automation):  This file contains the yaml content that is used for setting up the ci in GitLab.
  It will get triggered when the code is pushed to develop/master branch ( It can be further optimized for different pipelines to get triggered for different environments/branches.)
  The first job will create a docker image using the Dockerfile and will push it into the relevant docker registry.
  The second job will run the "terraform plan" first and will save the plan file as an artifact which will be used the in following step to apply the terrraform code to create the GKE cluster.
  The third job will trigger Helm Deploy using Helmfile. It will first setup the authentication with the gke cluster and will perform the helm deploy.
  The terraform deploy and Helm deploy Steps are kept manual so that the changes can be verified before applying.
  (Note: I have selected the relevant docker images that will be required to perform the specific functions and also installed extra packages if needed.)
  
  All the credentials are saved as environment variables as GitLab Env Vars that provides a secure way to pass variables.
  For every change in the Dockerfile, Kubernetes will perform a rolling update for the new image.
  Currently the Application is not having the scaling feature but we can add a HPA to enable the pod scaling.

Triggering the Automation:

1) Go to CI/CD --> Pipelines.

2) Click on Run Pipeline and select master branch.

3) Once the pipeline is triggered, It will create separate jobs as defined in the .gitlab-ci.yml file. (https://gitlab.com/asingh01/mediawiki/-/pipelines/256750986).

4) The Package step will create a docker image using the Dockerfile in the repo and will push it into a docker repository. The full job output can be viewed at https://gitlab.com/asingh01/mediawiki/-/jobs/1032305101

5) The next 2 jobs will perform Terraform Plan and Apply for the terraform code written in the terraform folder. It will create the VPC, GKE cluster, and the relevant resources required to get the GKE cluster up.

The terraform apply output is: https://gitlab.com/asingh01/mediawiki/-/jobs/1032336763

6) Once it is completed, The next 2 jobs will be responsible for deploying the MediaWiki application into the GKE Cluster. The Helm lint step will just lint the templates and the Helm Deploy step will deploy all the Kubernetes resources (Deployment, Service, Ingress) and once it is completed the application will be accessible via the ingress LoadBalancer IP.
The job link for helm deploy is: https://gitlab.com/asingh01/mediawiki/-/jobs/1032305105
